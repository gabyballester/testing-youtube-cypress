// Contexto engloba todo el test que dentro tiene dos pruebas
context('Navegación por Google y búsqueda en Youtube', () => { //El nombre del objetivo lo definimos aquí

    // Con 'it' definimos una prueba y damos nombre a la misma
    it('Visitar la página de Google', () => {
        //Con la palabra clave cy y el metodo visit y la página a visitar
        cy.visit('https://www.google.com/');
    });

    it('Escritura de Youtube en input', () => {
        //selector del input de google
        cy.get('.gLFyf').type('Youtube'); //Navegador introduce palabra en el input
    });

    it('Hacemos click en botón de búsqueda de google para buscar dicha palabra', () => {
        // click en el botón de búsqueda
        cy.get('.aajZCb > .tfB0Bf > center > .gNO89b').click();
    });

    it('Hacer click primer enlace y entrar en la web', () => {
        // Hacemos click en el primer elemento de la búsqueda
        cy.get('[href="https://www.youtube.com/?hl=es&gl=ES"] > .LC20lb').click();
    });
});




/**
// Contexto engloba todo el test que dentro tiene dos pruebas
context('Navegación por youtube', () => { //El nombre del objetivo lo definimos aquí

    // Tenemos 2 pruebas:

        // Con 'it' creamos nuestra 1º Prueba
        it('Visitar la página', () => {

            //Con la palabra clave cy y el metodo visit y la página a visitar
            cy.visit('https://www.google.com/');

        });

        // Creamos una segunda prueba
        it('Nombre De La Prueba 2', () => {

            // Vistar página google
            cy.visit('https://www.google.com/');

            // Obtenemos un elemento con un selector de clase y que escriba la palabra GeeksHubs
            cy.get('.gLFyf').type('GeeksHubs'); //Navegador introduce palabra en el input

            // Obtenemos el elemento del botón y hacemos clic
            cy.get('.aajZCb > .tfB0Bf > center > .gNO89b').click();

            // Hacemos click en el primer elemento de la búsqueda
            cy.get('[href="https://geekshubs.com/"] > .LC20lb').click();

        });

    });
    */